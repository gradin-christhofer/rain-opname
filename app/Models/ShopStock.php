<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopStock extends Model
{
    use HasFactory;
    public static $COLUMN_STOCK;
    public static $COLUMN_PRODUCT_CODE;
    
    // protected $connection = 'mysqlShop';
    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to faciliate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        $this->setConnection(config('database.shop_connection'));
        $this->table = env('DB_TABLE_ITEM');
        self::$COLUMN_PRODUCT_CODE = env('DB_TABLE_ITEM_COLUMN_PRODUCT_CODE');
        self::$COLUMN_STOCK = env('DB_TABLE_ITEM_COLUMN_STOCK');
    }
}
