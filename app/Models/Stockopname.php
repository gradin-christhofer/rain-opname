<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stockopname extends Model
{
    use HasFactory;

    public function submit()
    {
        return $this->hasMany(StockopnameSubmit::class);
    }

    public function detail()
    {
        return $this->hasManyThrough('App\Models\StockopnameDetail', 'App\Models\StockopnameSubmit');
    }

    public function compare()
    {
        return $this->hasMany(StockopnameCompare::class);
    }
}
