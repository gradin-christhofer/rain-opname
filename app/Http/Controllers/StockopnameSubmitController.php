<?php

namespace App\Http\Controllers;

use App\Events\StockopnameSubmitEvent;
use App\Http\Resources\ApiCollection;
use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockopnameSubmitController extends Controller
{
    /**
     * Store a newly created resource in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function store(Request $request)
    {
        $submit = new StockopnameSubmit();
        $submit->username = $request->username;
        $submit->save();

        $details = array_map(function ($item) {
            $detail = new StockopnameDetail();
            $detail->product_code = $item;
            return $detail;
        }, $request->items);
        $submit->detail()->saveMany($details);
        // event(new StockopnameSubmitEvent($submit, $request->items));
        return $submit->id;
    }

    /**
     * Test StockopnameSubmitController@show
     * Display stockopname_submits where id x, has many detail, with count detail.
     *
     * @param  \App\Models\StockopnameSubmit  $stockopnameSubmit
     * @return \Illuminate\Http\Response
     */
    public function show(StockopnameSubmit $stockopnameSubmit)
    {
        $result = StockopnameSubmit::
            where('id', $stockopnameSubmit->id)
            ->with('detailGrouped')
            ->orderBy('created_at', 'DESC')
            ->paginate();
            
        return new ApiCollection($result);
    }
}
