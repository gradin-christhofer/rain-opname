<?php

namespace App\Http\Controllers;

use App\Http\Resources\ApiCollection;
use App\Models\ShopStock;
use Illuminate\Http\Request;

class ShopStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShopStock  $shopStock
     * @return \Illuminate\Http\Response
     */
    public function show(ShopStock $shopStock)
    {
        $result = ShopStock::
            whereNotNull('id')
            ->paginate();
            
        return new ApiCollection($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShopStock  $shopStock
     * @return \Illuminate\Http\Response
     */
    public function edit(ShopStock $shopStock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShopStock  $shopStock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShopStock $shopStock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShopStock  $shopStock
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopStock $shopStock)
    {
        //
    }
}
