<?php

namespace App\Http\Controllers;

use App\Http\Resources\ApiCollection;
use App\Models\ShopStock;
use App\Models\Stockopname;
use App\Models\StockopnameCompare;
use App\Models\StockopnameSubmit;
use App\Models\StockopnameDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockopnameController extends Controller
{
    /**
     * Test StockopnameController@index
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result =  Stockopname::
            withCount('submit')
            ->withCount('detail')
            ->orderBy('created_at', 'DESC')
            ->paginate();
            
        return new ApiCollection($result);
    }

    /**
     * Test StockopnameController@store
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $chunk = 10000000;
        $stockopname = new Stockopname();
        $shop_stock = new ShopStock();
        $stockopname->save();
        $id = $stockopname->id;
        StockopnameSubmit::whereNull('stockopname_id')->update(['stockopname_id' => $id]);
        $details = StockopnameSubmit::where('stockopname_id', $id)
            ->leftjoin('stockopname_details', 'stockopname_submits.id', '=', 'stockopname_details.stockopname_submit_id')
            ->select('stockopname_details.product_code as product_code', DB::raw('COUNT(product_code) as stock'))
            ->groupBy('stockopname_details.product_code')
            ->havingRaw('COUNT(stockopname_details.product_code) > ?', [0])
            ->get();

        $detail_chunk = $details->chunk($chunk);
        $compares = collect([]);
        $all_product_code = [];
        foreach ($detail_chunk as $detailchunk) {
            $product_code_list = $detailchunk->pluck('product_code');
            $all_product_code = array_merge($all_product_code, $product_code_list->toArray());
            $stockdb = ShopStock::select(ShopStock::$COLUMN_PRODUCT_CODE, ShopStock::$COLUMN_STOCK)
                        ->whereIn(ShopStock::$COLUMN_PRODUCT_CODE, $product_code_list)->get();
            $stockdb_product_code = $stockdb->pluck(ShopStock::$COLUMN_PRODUCT_CODE);
            $comparing = $detailchunk->map(function ($detail) use ($stockdb, $stockdb_product_code) {
                $index_product_code = $stockdb_product_code->search($detail->product_code);
                $compare = new StockopnameCompare();
                $compare->product_code = $detail->product_code;
                $compare->stockopname = $detail->stock;
                $compare->stockdb = $index_product_code ? $stockdb[$index_product_code][ShopStock::$COLUMN_STOCK] : 0;
                return $compare;
            });
            $compares = $compares->merge($comparing);
        }

        $stockdb = ShopStock::whereNotIn(ShopStock::$COLUMN_PRODUCT_CODE, $all_product_code)
                    ->where(ShopStock::$COLUMN_STOCK, '>', 0)->get();
        $comparing = $stockdb->map(function ($stock) {
            $compare = new StockopnameCompare();
            $compare->product_code = $stock[ShopStock::$COLUMN_PRODUCT_CODE];
            $compare->stockopname = 0;
            $compare->stockdb = $stock[ShopStock::$COLUMN_STOCK];
            return $compare;
        });
        $compares = $compares->merge($comparing);
        $stockopname->compare()->saveMany($compares);
        return ['id' => $stockopname->id, 'created_at' => $stockopname->created_at];
    }

    /**
     * Test StockopnameController@show
     * Display the specified resource.
     *
     * @param  \App\Models\Stockopname  $stockopname
     * @return \Illuminate\Http\Response
     */
    public function show(Stockopname $stockopname)
    {
        $result = StockopnameSubmit::
            where('stockopname_id', $stockopname->id)
            ->withCount('detail')
            ->orderBy('created_at', 'DESC')
            ->paginate();
            
        return new ApiCollection($result);
    }

    /**
     * Test StockopnameController@destroy
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stockopname  $stockopname
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $details = StockopnameSubmit::with('detail')->whereNull('stockopname_id')->get();
        if ($details->count() > 0) {
            $ids = [];
            foreach ($details as $detail) {
                $ids = array_merge($ids, $detail->detail->pluck('id')->toArray());
            }
            StockopnameDetail::destroy($ids);
            StockopnameSubmit::whereNull('stockopname_id')->delete();
            return 'Success';
        }else{
            return 'Data is empty';
        }
    }

    public function print($stockopname_id, $state='view')
    {
        $today = Carbon::now()->translatedFormat('l, d F Y');
        
        $category = [
            ['class' => 'stockopname-less', 'category' => 'less', 'title' => 'Kurang', 'color_head' => '#FF5A5F', 'color_row' => '#ffe6e6'],
            ['class' => 'stockopname-more', 'category' => 'more', 'title' => 'Lebih', 'color_head' => '#52B788', 'color_row' => '#edf7f3'],
            ['class' => 'stockopname-missed', 'category' => 'missed', 'title' => 'Terlewat', 'color_head' => '#FFB30C', 'color_row' => '#fff7e6'],
            ['class' => 'stockopname-new', 'category' => 'new', 'title' => 'Tidak Terdaftar', 'color_head' => '#4EA8DE', 'color_row' => '#e9f4fb'],
        ];

        $stockopname = Stockopname::find($stockopname_id)->load('submit', 'compare');
        $sorted_result = [
            'more' => [], 'less' => [], 'new' => [], 'missed' => []
        ];
        foreach($stockopname->compare as $data){
            if($data->stockopname > $data->stockdb){
                if($data->stockdb==0){
                    $sorted_result['new'][] = $data;
                }else{
                    $sorted_result['more'][] = $data;
                }
            }elseif($data->stockopname < $data->stockdb){
                if($data->stockopname==0){
                    $sorted_result['missed'][] = $data;
                }else{
                    $sorted_result['less'][] = $data;
                }
            }
        }

        $list_pic = implode(', ', $stockopname->submit->pluck('username')->all());

        $pdf = app('dompdf.wrapper')->loadView('pdf.stockopname', [
            'sorted_result' => $sorted_result,
            'list_pic' => $list_pic,
            'date' => $stockopname->created_at->isoFormat('dddd, D MMMM Y'),
            'today' => $today,
            'category' => $category
        ]);
        $pdf->setPaper('a4', 'portrait');
        $dom_pdf = $pdf->getDomPDF();
        $canvas = $dom_pdf->get_canvas();
        $x = 520; //x coordinate
        $y = 800; //y coordinate
        $text = "Lembar {PAGE_NUM} / {PAGE_COUNT}";
        $text_font = null;
        $text_size = 10;
        $canvas->page_text($x, $y, $text, $text_font, $text_size, array(0, 0, 0));
        
        if($state=='view'){
            return view('pdf.stockopname', [
                'sorted_result' => $sorted_result,
                'list_pic' => $list_pic,
                'date' => $stockopname->created_at->isoFormat('dddd, D MMMM Y'),
                'today' => $today,
                'category' => $category
            ]);
        }elseif($state=='viewPdf'){
            return $pdf->stream('laporan-stockopname-' . $stockopname->created_at->isoFormat('dddd, D MMMM Y') . '.pdf');
        }else{
            return $pdf->download('laporan-stockopname-' . $stockopname->created_at->isoFormat('dddd, D MMMM Y') . '.pdf');
        }
    }
}
