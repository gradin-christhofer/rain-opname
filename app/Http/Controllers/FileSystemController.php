<?php

namespace App\Http\Controllers;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class FileSystemController extends Controller
{
    public function download(){
        //git pull, update all files
        $process = Process::fromShellCommandline('git pull origin main');
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        
        return $this->update();
    }

    public function update(){
        // file location at root folder
        $list_command = [];
        $result = [];
        foreach(file(base_path('update.txt')) as $line) {
            $list_command[] = trim(preg_replace('/\s\s+/', ' ', $line));
        }

        foreach($list_command as $command){
            $process = Process::fromShellCommandline($command);
            $process->run();

            // executes after the command finishes
            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            $result[] = [
                'command' => $command,
                'result' => $process->getOutput()
            ];
        }

        return $result;
    }
}
