<?php

namespace Tests;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    /**
     * URL that will be used for json request.
     *
     * @var string
     */
    protected $url = '';

    /**
     * String date for testing now.
     *
     * @var string
     */
    private $now = '2020-01-24 06:00:00';

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        Carbon::setTestNow($this->now);
    }

    /**
     * Clean up the testing environment before the next test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        Carbon::setTestNow(); // clear mock

        parent::tearDown();
    }

    /**
     * Get carbon string to assert column created_at and updated_at is working.
     *
     * @return string
     */
    public function now()
    {
        // 2020-01-24T06:00:00.000000Z
        return Carbon::make($this->now)->toISOString();
    }

    /**
     * Set response GET for controller test index.
     *
     * @param  string  $suffix
     * @return \Illuminate\Testing\TestResponse
     */
    public function jsonGet(string $suffix = '')
    {
        return $this->json('get', $this->url . "/$suffix");
    }

    /**
     * Set response POST for controller test store.
     *
     * @param  array   $data
     * @param  string  $suffix
     * @return \Illuminate\Testing\TestResponse
     */
    public function jsonPost(array $data = [], string $suffix = '')
    {
        return $this->json('post', $this->url . "/$suffix", $data);
    }

    /**
     * Set response PUT for controller test update.
     *
     * @param  array   $form
     * @param  string  $suffix
     * @return \Illuminate\Testing\TestResponse
     */
    public function jsonPut(array $form, string $suffix = '')
    {
        return $this->json('put', $this->url . "/$suffix", $form);
    }

    /**
     * Set response DELETE for controller test destroy.
     *
     * @param  string  $suffix
     * @return \Illuminate\Testing\TestResponse
     */
    public function jsonDelete(string $suffix = '')
    {
        return $this->json('delete', $this->url . "/$suffix");
    }
}
