<?php

namespace Tests\Feature\Controller;

use App\Models\ShopStock;
use App\Models\Stockopname;
use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class StockopnameControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    protected $submit_count = 3; //remove the sequence if you want to add more than 3 submits
    protected $detail_count = 10;
    protected $product_code = 'A0001*B*XL';
    protected $username = [
        ['username' => 'Artic Uno'],
        ['username' => 'Zap Dos'],
        ['username' => 'Molten Tres']
    ]; //made up username
    
    public function testStockopnameDestroy()
    {
        $this->product_code = 'A0001*B*XL';

        //initialize shop db
        $shop = ShopStock::factory()
            ->count(14)
            ->state(new Sequence(
                ['product_code' => 'A0001*B*XL'],
                ['product_code' => 'A0002*B*XL'],
                ['product_code' => 'A0003*B*XL'],
                ['product_code' => 'A0004*B*XL'],
                ['product_code' => 'A0005*B*XL'],
                ['product_code' => 'B0001*B*XL'],
                ['product_code' => 'B0002*B*XL'],
                ['product_code' => 'B0004*B*XL'],
                ['product_code' => 'B0005*B*XL'],
                ['product_code' => 'C0001*B*XL'],
                ['product_code' => 'C0002*B*XL'],
                ['product_code' => 'C0003*B*XL'],
                ['product_code' => 'C0004*B*XL'],
                ['product_code' => 'C0005*B*XL']
            ))
            ->create();

        //submit
        $submit = StockopnameSubmit::factory()
            ->count($this->submit_count)
            ->hasDetail($this->detail_count)
            ->create();

        //delete method for deleting stockopname submit and detail. don't run with save method below
        $this->url = route('stockopname.destroy');
        $this->jsonPost();

        // check if data on submit and detail table has been ceompletely deleted
        $this->assertDatabaseCount('stockopname_details', 0);
        $this->assertDatabaseCount('stockopname_submits', 0);

        // $this->url = route('stockopname.print', ['stockopname_id' => 1]); //print
        // $response = $this->jsonGet(); //result query table stockopname_compares, where stockopname_id x
        // dd($response);
    }

    public function testStockopnameStore()
    {
        //initialize shop db
        $shop = ShopStock::factory()
            ->count(14)
            ->state(new Sequence(
                ['product_code' => 'A0001*B*XL'],
                ['product_code' => 'A0002*B*XL'],
                ['product_code' => 'A0003*B*XL'],
                ['product_code' => 'A0004*B*XL'],
                ['product_code' => 'A0005*B*XL'],
                ['product_code' => 'B0001*B*XL'],
                ['product_code' => 'B0002*B*XL'],
                ['product_code' => 'B0004*B*XL'],
                ['product_code' => 'B0005*B*XL'],
                ['product_code' => 'C0001*B*XL'],
                ['product_code' => 'C0002*B*XL'],
                ['product_code' => 'C0003*B*XL'],
                ['product_code' => 'C0004*B*XL'],
                ['product_code' => 'C0005*B*XL']
            ))
            ->create();

        $submit = StockopnameSubmit::factory()
            ->count($this->submit_count)
            ->state(new Sequence($this->username[0], $this->username[1], $this->username[2]))
            ->has(StockopnameDetail::factory()
                    ->count($this->detail_count)
                    ->state(new Sequence(['product_code' => $this->product_code])),
                    'detail'
                )
            ->create();
        
        $this->url = route('stockopname.store');
        $this->jsonPost();

        //check stockopname, stockopname_submit, stockopname_detail
        $this->assertDatabaseCount('stockopnames', 1);
        for($i=0;$i<$this->submit_count;$i++){
            $this->assertDatabaseHas('stockopname_submits', [
                'id' => $submit[$i]->id,
                'username' => $this->username[$i]['username']
            ]);
            for($j=0;$j<$this->detail_count;$j++){
                $this->assertDatabaseHas('stockopname_details', [
                    'stockopname_submit_id' => $submit[$i]->id,
                    'product_code' => $this->product_code
                ]);
            }
        }
        $this->assertDatabaseHas('stockopname_compares', [
            'product_code' => $this->product_code,
            'stockopname' => $this->submit_count * $this->detail_count
        ]);
    }

    public function testStockopnameIndex()
    {
        $submit = StockopnameSubmit::factory()
            ->count($this->submit_count)
            ->state(new Sequence($this->username[0], $this->username[1], $this->username[2]))
            ->has(StockopnameDetail::factory()
                    ->count($this->detail_count)
                    ->state(new Sequence(['product_code' => $this->product_code])),
                    'detail'
                )
            ->create();
        
        $this->url = route('stockopname.store');
        $this->jsonPost();
        
        //proses index, show, dan print stockopname
        $this->url = route('stockopname.index'); //index
        $response = $this->jsonGet()->assertJson([
            'data' => [[
                'submit_count' => $this->submit_count,
                'detail_count' => $this->submit_count * $this->detail_count,
            ]],
            'links' => [
                'first' => 'https://stockopname.test/stockopname?page=1',
                'last' => 'https://stockopname.test/stockopname?page=1'
            ],
            'meta' => [
                'current_page' => 1,
                'from' => 1,
                'last_page' => 1,
                'per_page' => 15,
                'to' => 1,
                'total' => 1,
            ]
        ]); //result query table stockopnames, with count submit, with count detail
    }

    public function testStockopnameShow()
    {
        $submit = StockopnameSubmit::factory()
            ->count($this->submit_count)
            ->state(new Sequence($this->username[0], $this->username[1], $this->username[2]))
            ->has(StockopnameDetail::factory()
                    ->count($this->detail_count)
                    ->state(new Sequence(['product_code' => $this->product_code])),
                    'detail'
                )
            ->create();
        
        $this->url = route('stockopname.store');
        $this->jsonPost();

        $last_row = DB::table('stockopnames')->latest('created_at')->first();

        $this->url = route('stockopname.show', $last_row->id); //show
        $response = $this->jsonGet()->assertJson([
            'data' => [
                ['username' => $this->username[0]['username'], 'detail_count' => $this->detail_count],
                ['username' => $this->username[1]['username'], 'detail_count' => $this->detail_count],
                ['username' => $this->username[2]['username'], 'detail_count' => $this->detail_count],
            ],
            'meta' => [
                'current_page' => 1,
                'from' => 1,
                'last_page' => 1,
                'per_page' => 15,
                'to' => 3,
                'total' => 3,
            ]
        ]); //result query table stockopname_submits, where id x, with count detail
    }
}