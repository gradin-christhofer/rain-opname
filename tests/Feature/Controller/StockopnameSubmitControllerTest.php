<?php

namespace Tests\Feature\Controller;

use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StockopnameSubmitControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function testStockopnameSubmitShow()
    {
        $detail_count = 3;
        $product_code = 'A0001*B*XL';

        //ceate random data using factory
        $submit = StockopnameSubmit::factory()
            ->has(StockopnameDetail::factory()
                    ->count($detail_count)
                    ->state(new Sequence(['product_code' => $product_code])),
                    'detail'
                )
            ->create();

        //show saved submit
        $this->url = route('stockopname_submit.show', ['stockopname_submit' => $submit]); //show
        $response = $this->jsonGet()->assertJson([
            'data'=> [[
                'id' => $submit->id,
                'username' => $submit->username,
                'detail_count' => $detail_count,
                'detail' => [
                    ['stockopname_submit_id' => $submit->id, 'product_code' => $product_code],
                    ['stockopname_submit_id' => $submit->id, 'product_code' => $product_code],
                    ['stockopname_submit_id' => $submit->id, 'product_code' => $product_code],
                ]
            ]],
            'meta' => [
                'current_page' => 1,
                'from' => 1,
                'last_page' => 1,
                'per_page' => 15,
                'to' => 1,
                'total' => 1,
            ]
        ]); //result query stockopname_submits where id x, has many detail, with count detail
    }
}