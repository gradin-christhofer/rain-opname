<?php

namespace Database\Factories;

use App\Models\ShopStock;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShopStockFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ShopStock::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $index = 1;
        return [
            'stoktoko' => rand(0, 500),
            'kode' => 'A' . $index++,
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
