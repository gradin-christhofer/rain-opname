import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: () => import(/* webpackChunkName: "desktop" */ '../views/Dashboard'),
    children: [
      { path: '', name: 'home', component: () => import(/* webpackChunkName: "desktop" */ '../views/Home') },
      { path: 'detail/:id_stockopname_submit', name: 'detail', component: () => import(/* webpackChunkName: "desktop" */ '../views/Detail') },
      { path: 'riwayat', name: 'riwayat', component: () => import(/* webpackChunkName: "desktop" */ '../views/Riwayat') },
      { path: 'riwayat_detail/:id_stockopname', name: 'riwayat_detail', component: () => import(/* webpackChunkName: "desktop" */ '../views/RiwayatDetail') },
    ],
  },
  { path: '/scan', name: 'scan', component: () => import(/* webpackChunkName: "mobile" */ '../views/ScanForm') },
  { path: '/update', name: 'update', component: () => import(/* webpackChunkName: "desktop" */ '../views/Update') },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router