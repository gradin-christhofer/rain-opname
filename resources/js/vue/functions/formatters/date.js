const locale = 'id-id'
export const dateShort = dt => { // format : 13 Jan 2020
  if (!dt) return '-'
  const config = {
    month: 'short',
    day: 'numeric',
    year: 'numeric',
  }
  return new Date(dt).toLocaleDateString(locale, config)
}
export const dateLong = dt => { // format : 13 Januari 2020
  if (!dt) return '-'
  const config = {
    month: 'long',
    day: 'numeric',
    year: 'numeric',
  }
  return new Date(dt).toLocaleDateString(locale, config)
}
export const dateTime = dt => { // format : Tuesday, 13 Jan 2020 17:24:38
  if (!dt) return '-'
  const config = {
    weekday: 'long',
    month: 'short',
    day: 'numeric',
    year: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  }
  return new Date(dt).toLocaleDateString(locale, config)
}
export const time = dt => { // format : 17:24
  if (!dt) return '-'
  const config = {
    hour: 'numeric',
    minute: 'numeric',
  }
  return new Date(dt).toLocaleTimeString('en-gb', config)
}
export default {
  dateLong,
  dateShort,
  dateTime,
  time,
}