import { number } from './number'
import { dateShort, dateLong, dateTime, time } from './date'
export default {
  number,
  dateLong,
  dateShort,
  dateTime,
  time,
}