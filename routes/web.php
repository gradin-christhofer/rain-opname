<?php

use App\Http\Controllers\FileSystemController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ShopStockController;
use App\Http\Controllers\StockopnameController;
use App\Http\Controllers\StockopnameSubmitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//redirect route bila tidak ditemukan laravel
Route::fallback(function () {
    return view('app');
});

Route::post('stockopname/destroy', [StockopnameController::class, 'destroy'])->name('stockopname.destroy');
Route::get('stockopname/print/{stockopname_id}/{state?}', [StockopnameController::class, 'print'])->name('stockopname.print');
Route::get('file_system/download', [FileSystemController::class, 'download'])->name('file_system.download');

Route::resource('stockopname', StockopnameController::class)->except([
    'destroy',
]);
Route::resource('stockopname_submit', StockopnameSubmitController::class);
Route::resource('shop_stock', ShopStockController::class);

Route::resource('home', HomeController::class);
